package com.GUArena.BCA;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.annotation.ElementType;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Result1_New extends AppCompatActivity {
    private static final String TAG = ResultAll.class.getSimpleName();
    Spinner spinner1;
    EditText etText,captchTxt;
    HashMap<String,String> Resultlist = new HashMap<String, String>();
    ArrayList<String> ResultArrayList = new ArrayList<String>();
    StringBuffer response = new StringBuffer();
    static StringBuffer student;
    public static  Connection conn;
    public static  Document d;
    String url;
    String lstExam;
    String txtSeatNo;
    String captchaCode;
    String sessionId ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result1__new);
        Log.i(TAG , "Instance created For Result 1");
        etText = findViewById(R.id.seatNo);
        captchTxt = findViewById(R.id.captchaEnter);
        new Result1_New.PostJSONTask().execute();

    }
    public void addItemsOnSpinner1() {

        spinner1 = (Spinner) findViewById(R.id.spinner1);



        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, ResultArrayList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter);
    }
    public void displayResult(View view){
        TextView textView = (TextView)spinner1.getSelectedView();
        String result = textView.getText().toString();
        Log.i(TAG , "You selected " + result);
        String code = Resultlist.get(result);
        if(code == ""){
            code="0";
        }
        int codeInt = Integer.parseInt(code);
        txtSeatNo = etText.getText().toString().replaceFirst("^0+(?!$)", "");
        Log.i(TAG , "You selected Code String " + code);
        Log.i(TAG , "You selected Seat " + txtSeatNo);
        lstExam = code;
        captchaCode = captchTxt.getText().toString();

        if(code != "0"){
            if(!txtSeatNo.isEmpty() ) {
                if(!txtSeatNo.equals("0")) {
                    if(captchaCode.toString().length() > 0) {
                        Log.i(TAG , "You selected Code int " + codeInt);

                        Log.i(TAG , "Your Seat " + txtSeatNo);
                        new FetchResult().execute();
                    }else {
                        Toast tstMsg = Toast.makeText(getApplicationContext(),"Please Enter Captcha ",Toast.LENGTH_LONG);
                        tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
                        tstMsg.show();
                    }
                }else {
                    Toast tstMsg = Toast.makeText(getApplicationContext(),"Please Enter Seat Number properly ",Toast.LENGTH_LONG);
                    tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
                    tstMsg.show();
                }



            }else {
                Toast tstMsg = Toast.makeText(getApplicationContext(),"Ha ha ha Please Input Seat Number",Toast.LENGTH_LONG);
                tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
                tstMsg.show();
            }
        }else{
            Toast tstMsg = Toast.makeText(getApplicationContext(),"Please Select Course First",Toast.LENGTH_LONG);
            tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
            tstMsg.show();
        }


    }
    private class PostJSONTask extends AsyncTask<String, String, String> {
        final String TAG = Result1_New.PostJSONTask.class.getSimpleName();

        ProgressBar spinner = findViewById(R.id.spinner);
        int responseCode;
        URL obj;
        StringBuilder tokenUri;
        HttpURLConnection con = null;
        private final String USER_AGENT = "Mozilla/5.0";
        Bitmap bp;
        @Override
        protected String doInBackground(String... params) {
            try {
            conn = Jsoup.connect(BCA_FrontPage.GUJ_URL_RESULT1  );
            d = null;
                do{
                    d = conn.get();
                }while(d == null  );



            Element captcha =  d.select("img.control-label").first();
            if (captcha == null) {
                throw new RuntimeException("Unable to find captcha...");
            }
                Elements lists =  d.select("option");
                System.out.println(lists);
                for(Element l : lists){
                    System.out.println(l.attr("value"));
                    Resultlist.put(l.text(),l.attr("value"));
                    ResultArrayList.add(l.text());
                }
            // Fetch the captcha image
            Connection.Response response = null;

                response = Jsoup //
                        .connect(captcha.absUrl("src")) // Extract image absolute URL
                        .cookies(conn.response().cookies()) // Grab cookies
                        .ignoreContentType(true) // Needed for fetching image
                        .execute();

                sessionId = conn.response().cookie("PHPSESSID");
                System.out.println("Session Id " +sessionId);
            // Load image from Jsoup response
//            ImageIcon image = new ImageIcon(ImageIO.read(new ByteArrayInputStream(response.bodyAsBytes())));
                System.out.println("BODY AS  BYTE gotchaaaa : "+response.bodyAsBytes().toString());
                ByteArrayInputStream bis = new ByteArrayInputStream(response.bodyAsBytes());
                System.out.println(response.body().toString()+"   gotchaaaa 1");
                  bp= BitmapFactory.decodeStream(bis);
                System.out.println(bp.getRowBytes()+"   gotchaaaa 1");
                int num;
                while( (num = bis.read()) != -1 ) {
                    System.out.println(bis.read()+"   gotchaaaa 1");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }


        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            dialog = ProgressDialog.show(Result1_New.this, "", "Loading...", true);


//            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.print(TAG + s);
            ImageView logoimg = (ImageView) findViewById(R.id.captchacode);
            logoimg.setImageBitmap(bp);
            addItemsOnSpinner1();
            dialog.dismiss();

        }
    }
    private class FetchResult extends AsyncTask<String, String, String> {
        final String TAG = FetchResult.class.getSimpleName();

//        ProgressBar spinner = findViewById(R.id.spinner1);
        int responseCode;
        URL obj;
        StringBuilder tokenUri;
        private final String USER_AGENT = "Mozilla/5.0";
        Document doc1 = null;
        @Override
        protected String doInBackground(String... params) {
            String txt="";

            System.out.println(TAG + lstExam);
            System.out.println(TAG + txtSeatNo);
            System.out.println(TAG + captchaCode);
            System.out.println(TAG + sessionId);
            try {
                doc1 = null;
                while(doc1 == null  )
                {
                    doc1 = conn.data("exam", lstExam)
                            .data("seat", txtSeatNo)
                            .data("vercode", captchaCode)
                            .data("rsl", "")
                            .cookie("PHPSESSID", sessionId)
                            .post();
                }
                Element emt = doc1.selectFirst("div.alert-danger");

                Log.i(TAG , "emt is " + emt);
               if(emt == null){
                   txt= doc1.select("pre").first().text();
                   System.out.print(TAG +txt);
                   Log.i(TAG , "Go On " );
               }else{
                   Log.i(TAG , "emt is " + emt.text());
//                   Toast tstMsg = Toast.makeText(getApplicationContext(),emt.text(),Toast.LENGTH_LONG);
//                   tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
//                   tstMsg.show();
                   txt= doc1.select("div.alert-danger").first().text();
                   return txt;
               }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return txt;


        }


        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            dialog = ProgressDialog.show(Result1_New.this, "", "Loading...", true);


//            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            System.out.print(TAG + s);
            dialog.dismiss();
            student = new StringBuffer(s.toString());
            String rslt =  student.toString();

            if (rslt.isEmpty()) {

                Toast tstMsg = Toast.makeText(getApplicationContext(),"Something Went Wrong !",Toast.LENGTH_LONG);
                tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
                tstMsg.show();

            }else if(rslt.indexOf("Invalid or Wrong Seat Number.") != -1) {

                Toast tstMsg = Toast.makeText(getApplicationContext(),"Enter Valid Seat Number",Toast.LENGTH_LONG);
                tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
                tstMsg.show();
            }else if(rslt.indexOf("Incorrect Captcha code.") != -1) {

                Toast tstMsg = Toast.makeText(getApplicationContext(),"Incorrect Captcha code.",Toast.LENGTH_LONG);
                tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
                tstMsg.show();
            }else {
                startActivity(new Intent(Result1_New.this, Show_Result1_New.class));
            }

        }
    }
}

