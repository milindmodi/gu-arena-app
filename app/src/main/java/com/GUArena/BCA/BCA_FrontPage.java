package com.GUArena.BCA;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import javax.xml.transform.OutputKeys;

public class BCA_FrontPage extends AppCompatActivity {

    private static final String PREFS_NAME = "PREFS_NAME" ;
    public static boolean isbgServiceOn = false;
    private static final String TAG = BCA_FrontPage.class.getSimpleName();
    GridLayout mainGrid;
    TextView landing;
    public static final String GUJ_URL_SYLLABUS = "https://www1.gujaratuniversity.ac.in/custom/student/syllabus";
    public static final String GUJ_URL_TIMETABLE = "https://www1.gujaratuniversity.ac.in/custom/student/examination-schedule";

    public static final String GUJ_URL_CANDIDATE = "https://www1.gujaratuniversity.ac.in/custom/student/candidate-list";
    public static final String GUJ_URL_OTHERRESULT = "https://www1.gujaratuniversity.ac.in/custom/student/oher-results";
    public static final String GUJ_URL_RESULT1 = "http://result.gujaratuniversity.ac.in/";
    public  static  boolean isConnected(Context ct){
        ConnectivityManager cm = (ConnectivityManager) ct.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
            return false;
        } else
        return false;
    }

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        
        setContentView(R.layout.bca_frontpage);
        if (!isConnected(this)) {
            AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                    this);
            alertDialog2.setTitle(R.string.warning_alert3);


            alertDialog2.setPositiveButton(R.string.warning_alert4,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            Intent intent = new Intent(getApplicationContext(), BCA_FrontPage.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    });
            alertDialog2.show();
        }

        TextView textView =(TextView)findViewById(R.id.terms);
        textView.setClickable(true);







        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        isbgServiceOn = settings.getBoolean("isbgServiceOn",false);
        if( isbgServiceOn ){
            startService(new Intent(BCA_FrontPage.this , BackgroundService.class));
            Log.i(TAG , "BG RUNNIG");
        }
        else{
            Log.i(TAG , "BG not RUNNIG");
        }


        mainGrid = findViewById(R.id.main_grid);
        buttonInAction(mainGrid);
      //  startService(new Intent(BCA_FrontPage.this,AlertService.class));
    }
    public void openLink(View v){
        Log.e(TAG,"Error in calling ");
        String link = "https://gujarat-university-arena.web.app";
        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(myIntent);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (!isConnected(this)) {
            Toast t = Toast.makeText(BCA_FrontPage.this, "Connect Internet First", Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER,0,0);
            t.show();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor ed = settings.edit();
        ed.putBoolean("isbgServiceOn",isbgServiceOn);
        ed.commit();
        Log.i(TAG , "BG SAVED");
    }

    @Override
    protected void  onRestart() {

        super.onRestart();
        defaultBG(mainGrid);
    }


    private void defaultBG(final  GridLayout mainGrid){
        for(int i=0; i < mainGrid.getChildCount(); i++){
            final CardView btn = (CardView) mainGrid.getChildAt(i);
            btn.setCardBackgroundColor(Color.parseColor(String.valueOf("white")));
        }
    }

    public void sem_6(View v){
        Results.lay = 6;
        startActivity(new Intent(BCA_FrontPage.this , Results.class ));
    }

    @SuppressLint("ResourceAsColor")
    private void buttonInAction (final GridLayout mainGrid){
       for(int i=0; i < mainGrid.getChildCount(); i++){
          final CardView btn = (CardView) mainGrid.getChildAt(i);
            final int FI = i;
           btn.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Dialog dialog;
                   btn.setCardBackgroundColor(R.color.btnBackground);
                   switch (FI){
                       case 0:
                           Log.i(TAG , "From homepage selected Button " + FI);
                           startActivity(new Intent(BCA_FrontPage.this ,ResultAll.class));
                           break;
                       case 1:
                           Log.i(TAG , "From homepage selected Button " + FI);
                           startActivity(new Intent(BCA_FrontPage.this ,TimeTableHome.class));
                           break;
                       case  2:
                           Log.i(TAG , "From homepage selected Button " + FI);
                           startActivity(new Intent(BCA_FrontPage.this ,CandidateListHome.class));
                           break;
                       case 3:
//                           AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
//                                   BCA_FrontPage.this);
//                           alertDialog2.setTitle(R.string.future1);
//                           alertDialog2.setMessage(R.string.future2);
//
//
//                           alertDialog2.setPositiveButton("YES",
//                                   new DialogInterface.OnClickListener() {
//                                       public void onClick(DialogInterface dialog, int which) {
//                                           // Write your code here to execute after dialog
//                                           Toast.makeText(getApplicationContext(),
//                                                   "", Toast.LENGTH_SHORT)
//                                                   .show();
//                                       }
//                                   });
//                           alertDialog2.show();
//                           btn.setCardBackgroundColor(Color.parseColor(String.valueOf("white")));

                       case 4:
                           Log.i(TAG , "From homepage Syllabus Button selected");
                           startActivity(new Intent(BCA_FrontPage.this,SyllabusHome.class));
                           break;

//                       case 5:
//                           Log.i(TAG , "From Alert Button selected");
//                           startActivity(new Intent(BCA_FrontPage.this,AlertHome.class));
//                           break;





                   }

               }
           });

       }
    }
}

