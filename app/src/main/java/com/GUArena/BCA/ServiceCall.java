package com.GUArena.BCA;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class ServiceCall extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
//        Toast.makeText(/**/this,"hello",Toast.LENGTH_SHORT).show();
        createNotification();
    }


    private void createNotification() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel seviceChannel = new NotificationChannel(
                    "0",
                    "Example",
                    NotificationManager.IMPORTANCE_DEFAULT
            );


            NotificationManager notificationManager   = getSystemService(NotificationManager.class);
                notificationManager.createNotificationChannel(seviceChannel);
            }
        }
}
