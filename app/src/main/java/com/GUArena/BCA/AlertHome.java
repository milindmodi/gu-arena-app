package com.GUArena.BCA;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import static com.GUArena.BCA.BCA_FrontPage.isConnected;

public class AlertHome extends AppCompatActivity {
    private static final String PREFS_NAME = "PREFS_NAME";
    private static final String TAG = AlertHome.class.getSimpleName();
    TextView homepage;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        homepage = findViewById(R.id.home_title);
        homepage.setText(R.string.alertButton);

    }



    Switch mySwitch1;
    Switch mySwitch2;
    Switch mySwitch3;
    Switch mySwitch4;
    Switch mySwitch5;
    Switch mySwitch6;
    boolean silent1;
    boolean silent2;
    boolean silent3;
    boolean silent4;
    boolean silent5;
    boolean silent6;

    @Override
    protected void onDestroy() {
        super.onDestroy();
   commitWork();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG   , "ALERT HOME CREATED" );

        setContentView(R.layout.alert_homepage);
        if (!isConnected(this)) {
            AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                    this);
            alertDialog2.setTitle(R.string.warning_alert3);


            alertDialog2.setPositiveButton(R.string.warning_alert4,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            finish();
                            startActivity(new Intent(AlertHome.this,BCA_FrontPage.class));
                        }
                    });
            alertDialog2.show();
            Log.i(TAG   , "ALERT HOME warning" );

        }
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        String Warning = settings.getString("Warning","OKAY");
        if( Warning.equals("OKAY")) {
            AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                    this);
            alertDialog2.setTitle(R.string.warning_alert1);
            alertDialog2.setMessage(R.string.warning_alert2);


            alertDialog2.setPositiveButton("Okay",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            Toast.makeText(getApplicationContext(),
                                    "", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    });
            alertDialog2.show();
            Warning = "NONE";
            SharedPreferences stg = getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor warn =  stg.edit();
            warn.putString("Warning",Warning);
            warn.commit();
        }


        setContentView(R.layout.alert_homepage);

        mySwitch1 = findViewById(R.id.sem1_alert);
        mySwitch2 = findViewById(R.id.sem2_alert);
        mySwitch3 = findViewById(R.id.sem3_alert);
        mySwitch4 = findViewById(R.id.sem4_alert);
        mySwitch5 = findViewById(R.id.sem5_alert);
        mySwitch6 = findViewById(R.id.sem6_alert);

      mySwitch1.setChecked(settings.getBoolean("switchkey1",false));
      mySwitch2.setChecked(settings.getBoolean("switchkey2",false));
      mySwitch3.setChecked(settings.getBoolean("switchkey3",false));
      mySwitch4.setChecked(settings.getBoolean("switchkey4",false));
      mySwitch5.setChecked(settings.getBoolean("switchkey5",false));
      mySwitch6.setChecked(settings.getBoolean("switchkey6",false));


        mySwitch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {


                if (isChecked) {
                    Log.i(TAG   , "ALERT HOME CREATED" );
                    Log.i(TAG, "Sem 1 results will be notified");
                    Toast msg = Toast.makeText(AlertHome.this, "Sem 1 results will be notified", Toast.LENGTH_LONG);
                    msg.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    msg.show();
                    Intent service1 = new Intent(AlertHome.this, BackgroundService.class);
                    startService(service1);
                    Log.i(TAG   , "ALERT HOME Called service" );

                } else {
                    Toast msg = Toast.makeText(AlertHome.this, "Alert Disabled", Toast.LENGTH_LONG);
                    msg.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    msg.show();
                }
                commitWork();

            }
        });


        mySwitch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {


                if (isChecked) {
                    Toast msg = Toast.makeText(AlertHome.this, "Sem 2 results will be notified", Toast.LENGTH_LONG);
                    ;
                    msg.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    msg.show();
                    Intent service1 = new Intent(AlertHome.this, BackgroundService.class);
                    startService(service1);
                } else {
                    Toast msg = Toast.makeText(AlertHome.this, "Alert Disabled", Toast.LENGTH_LONG);
                    ;
                    msg.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    msg.show();
                }
                commitWork();

            }
        });

        mySwitch3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    Toast msg = Toast.makeText(AlertHome.this, "Sem 3 results will be notified", Toast.LENGTH_LONG);
                    ;
                    msg.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    msg.show();
                    Intent service1 = new Intent(AlertHome.this, BackgroundService.class);
                    startService(service1);
                } else {
                    Toast msg = Toast.makeText(AlertHome.this, "Alert Disabled", Toast.LENGTH_LONG);
                    ;
                    msg.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    msg.show();
                }

            commitWork();
            }
        });

        mySwitch4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    Toast msg = Toast.makeText(AlertHome.this, "Sem 4 results will be notified", Toast.LENGTH_LONG);
                    ;
                    msg.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    msg.show();
                    Intent service1 = new Intent(AlertHome.this, BackgroundService.class);
                    startService(service1);
                } else {
                    Toast msg = Toast.makeText(AlertHome.this, "Alert Disabled", Toast.LENGTH_LONG);
                    ;
                    msg.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    msg.show();
                }
                commitWork();

            }
        });
        mySwitch5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {


                if (isChecked) {
                    Toast msg = Toast.makeText(AlertHome.this, "Sem 5 results will be notified", Toast.LENGTH_LONG);
                    ;
                    msg.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    msg.show();
                    Intent service1 = new Intent(AlertHome.this, BackgroundService.class);
                    startService(service1);
                } else {
                    Toast msg = Toast.makeText(AlertHome.this, "Alert Disabled", Toast.LENGTH_LONG);
                    msg.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    msg.show();
                }
                commitWork();
            }
        });

        mySwitch6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {



                if (isChecked) {
                    Toast msg = Toast.makeText(AlertHome.this, "Sem 6 results will be notified", Toast.LENGTH_LONG);
                    msg.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    msg.show();
                    Intent service1 = new Intent(AlertHome.this, BackgroundService.class);
                    startService(service1);
                } else {
                    Toast msg = Toast.makeText(AlertHome.this, "Alert Disabled", Toast.LENGTH_LONG);

                    msg.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                    msg.show();
                }
                commitWork();

            }
        });

    }

    private void commitWork() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("switchkey1", mySwitch1.isChecked());
        editor.putBoolean("switchkey2", mySwitch2.isChecked());
        editor.putBoolean("switchkey3", mySwitch3.isChecked());
        editor.putBoolean("switchkey4", mySwitch4.isChecked());
        editor.putBoolean("switchkey5", mySwitch5.isChecked());
        editor.putBoolean("switchkey6", mySwitch6.isChecked());
        editor.commit();
    }

    public void disableAll(View v){

            mySwitch1.setChecked(false);
            mySwitch2.setChecked(false);
            mySwitch3.setChecked(false);
            mySwitch4.setChecked(false);
            mySwitch5.setChecked(false);
            mySwitch6.setChecked(false);

            Intent ser = new Intent(this,BackgroundService.class);
            stopService(ser);
            SharedPreferences settings = getSharedPreferences(PREFS_NAME,0);
            SharedPreferences.Editor ed = settings.edit();
            ed.putBoolean("isbgServiceOn",false);
            ed.commit();
            Button txt = findViewById(R.id.button);
            Toast.makeText(this,"Disabled ",Toast.LENGTH_SHORT).show();
        }
    }