package com.GUArena.BCA;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

public class Result2_New extends AppCompatActivity {
    private static final String TAG = ResultAll.class.getSimpleName();
    Spinner spinner1;
    EditText etText,captchTxt;
    HashMap<String,String> Resultlist = new HashMap<String, String>();
    ArrayList<String> ResultArrayList = new ArrayList<String>();
    StringBuffer response = new StringBuffer();
    static StringBuffer student;
    public static Connection conn;
    public static Document d;
    String url;
    String lstExam;
    String txtSeatNo;
    String captchaCode;
    String sessionId ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result2__new);
        Log.i(TAG , "Instance created For Result 1");
        etText = findViewById(R.id.seatNo);
        new PostJSONTask().execute();
    }
    public void addItemsOnSpinner1() {

        spinner1 = (Spinner) findViewById(R.id.spinner1);



        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, ResultArrayList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter);
    }
    public void displayResult(View view){
        TextView textView = (TextView)spinner1.getSelectedView();
        String result = textView.getText().toString();
        Log.i(TAG , "You selected " + result);
        String code = Resultlist.get(result);
        if(code == ""){
            code="0";
        }
        int codeInt = Integer.parseInt(code);

        Log.i(TAG , "You selected Code String " + code);
        Log.i(TAG , "You selected Code int " + codeInt);
        lstExam = code;
        txtSeatNo = etText.getText().toString().replaceFirst("^0+(?!$)", "");
        Log.i(TAG , "Your Seat " + txtSeatNo);
        new FetchResult().execute();

        if(code != "0"){
            if(!txtSeatNo.isEmpty() ) {
                if(!txtSeatNo.equals("0")) {
                    new FetchResult().execute();
                }else {
                    Toast tstMsg = Toast.makeText(getApplicationContext(),"Please Enter Seat Number properly ",Toast.LENGTH_LONG);
                    tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
                    tstMsg.show();
                }



            }else {
                Toast tstMsg = Toast.makeText(getApplicationContext(),"Please Enter Seat Number",Toast.LENGTH_LONG);
                tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
                tstMsg.show();
            }
        }else{
            Toast tstMsg = Toast.makeText(getApplicationContext(),"Please select Course first",Toast.LENGTH_LONG);
            tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
            tstMsg.show();
        }
    }
    private class PostJSONTask extends AsyncTask<String, String, String> {
        final String TAG = PostJSONTask.class.getSimpleName();

        ProgressBar spinner = findViewById(R.id.spinner);
        int responseCode;
        URL obj;
        StringBuilder tokenUri;
        HttpURLConnection con = null;
        private final String USER_AGENT = "Mozilla/5.0";
        Bitmap bp;
        @Override
        protected String doInBackground(String... params) {
            try {
                conn = Jsoup.connect("http://www.gujaratuniversity.org.in/result_e/result/result_up.asp");
                d = null;

                d = conn.get();
                System.out.print((d.toString()));
                Elements lists =  d.select("option");
                System.out.println(lists);
                for(Element l : lists){
                    System.out.println(l.attr("value"));
                    Resultlist.put(l.text(),l.attr("value"));
                    ResultArrayList.add(l.text());
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }


        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            dialog = ProgressDialog.show(Result2_New.this, "", "Loading...", true);


//            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.print(TAG + s);

            addItemsOnSpinner1();
            dialog.dismiss();

        }
    }
    private class FetchResult extends AsyncTask<String, String, String> {
        final String TAG = FetchResult.class.getSimpleName();

        //        ProgressBar spinner = findViewById(R.id.spinner1);
        int responseCode;
        URL obj;
        StringBuilder tokenUri;
        private final String USER_AGENT = "Mozilla/5.0";
        Document doc1;
        HttpURLConnection con = null;

        @Override
        protected String doInBackground(String... params) {
            url = "http://www.gujaratuniversity.org.in/result_e/result/ex"+lstExam+"/exam_chk.asp";
            Log.i(TAG ,"Url IS " +url);
            try {
                tokenUri = new StringBuilder("cmdSearch=");
                tokenUri.append(URLEncoder.encode("Search", "UTF-8"));
                tokenUri.append("&lstExam=");
                tokenUri.append(URLEncoder.encode(lstExam, "UTF-8"));
                tokenUri.append("&txtSeatNo=");
                tokenUri.append(URLEncoder.encode(txtSeatNo, "UTF-8"));

                Log.i(TAG ,"Parameter converted");
                obj = new URL(url);
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("HEAD");

                Log.i(TAG ,"URL active");
                con.setRequestMethod("POST");
                con.setRequestProperty("User-Agent", USER_AGENT);
                con.setRequestProperty("Accept-Language", "UTF-8");

                con.setDoOutput(true);

                Log.i(TAG ,"Ready to take responce");
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(con.getOutputStream());
                outputStreamWriter.write(tokenUri.toString());
                outputStreamWriter.flush();

                responseCode = con.getResponseCode();
                Log.i(TAG ,"Responce taken");
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + tokenUri);
                System.out.println("Response Code : " + responseCode);
                Log.i(TAG ,"Writing string");
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                response.setLength(0);
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine + "\n");
                }
                in.close();
                System.out.println((response));

                Log.i(TAG ,"end of String");
            }catch (Exception e) {
                Log.e(TAG, "exception : Error at the connection");
            }
            return response.toString();
        }


        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            dialog = ProgressDialog.show(Result2_New.this, "", "Loading...", true);


//            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            System.out.print(TAG + s);
            dialog.dismiss();
            Result1_New.student= new StringBuffer(response.toString());
            Result1_New.student = new StringBuffer(s.toString());
            String rslt =  Result1_New.student.toString();

            if (rslt.isEmpty()) {

                Toast tstMsg = Toast.makeText(getApplicationContext(),"Something Went Wrong !",Toast.LENGTH_LONG);
                tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
                tstMsg.show();

            }else if(rslt.indexOf("Invalid Seat Number.") != -1) {

                Toast tstMsg = Toast.makeText(getApplicationContext(),"Enter Valid Seat Number",Toast.LENGTH_LONG);
                tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
                tstMsg.show();
            }else {
                startActivity(new Intent(Result2_New.this, Show_Result1_New.class));

            }
        }
    }
}
