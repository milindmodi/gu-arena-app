package com.GUArena.BCA;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootUpService extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        /***** For start Service  ****/
        Intent myIntent = new Intent(context, AlertService.class);
        context.startService(myIntent);
    }
}