package com.GUArena.BCA;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;


public class Results extends  AppCompatActivity {
    private static final String TAG = Results.class.getSimpleName();
    StringBuffer response = new StringBuffer();
    static StringBuffer student;

    String url;
    String lstExam;
    String txtSeatNo;
    EditText etText;
   // ProgressDialog dialog = new ProgressDialog(this);

    static int lay;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putInt("lay",lay);
//        outState.putString("url",url);
//        outState.putString("txtSeatNo",txtSeatNo);
//        outState.putString("response",response.toString());
//        outState.putString("student",student.toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        lay = savedInstanceState.getInt("lay",lay);
        url = savedInstanceState.getString("url",url);
        txtSeatNo = savedInstanceState.getString("txtSeatNo",txtSeatNo);
        response = new StringBuffer (savedInstanceState.getString("response"));
        student = new StringBuffer (savedInstanceState.getString("student"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (lay == 1)
            setContentView(R.layout.result1);
        else if (lay == 2)
            setContentView(R.layout.result2);
        else if (lay == 3)
            setContentView(R.layout.result3);
        else if (lay == 4)
            setContentView(R.layout.result4);
        else if (lay == 5)
            setContentView(R.layout.result5);
        else if (lay == 6)
            setContentView(R.layout.result6);
        Log.i(TAG, "Instance created : " + lay);
        etText = findViewById(R.id.seatNo);
    }

    public void sem_1(View v) {
        if(etText.getText().toString().length()>0) {
            lstExam = "91";
            txtSeatNo = etText.getText().toString().replaceFirst("^0+(?!$)", "");
            url = "http://www.gujaratuniversity.org.in/result_e/result/ex91/exam_chk.asp";
            new PostJSONTask().execute();
        }else {
            Toast tstMsg = Toast.makeText(getApplicationContext(),"Ha ha ha Please Input Seat Number",Toast.LENGTH_LONG);
            tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
            tstMsg.show();
        }

    }

    public void sem_2(View v) {
        if(etText.getText().toString().length()>0) {
        lstExam = "92";
        txtSeatNo = etText.getText().toString().replaceFirst("^0+(?!$)", "");
        url = "http://www.gujaratuniversity.org.in/result_e/result/ex92/exam_chk.asp";
        new PostJSONTask().execute();
        }else {
            Toast tstMsg = Toast.makeText(getApplicationContext(),"Ha ha ha Please Input Seat Number",Toast.LENGTH_LONG);
            tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
            tstMsg.show();
        }
    }

    public void sem_3(View v) {
        if(etText.getText().toString().length()>0) {
        lstExam = "93";
        txtSeatNo = etText.getText().toString().replaceFirst("^0+(?!$)", "");
        url = "http://www.gujaratuniversity.org.in/result_e/result/ex93/exam_chk.asp";
        new PostJSONTask().execute();
        }else {
            Toast tstMsg = Toast.makeText(getApplicationContext(),"Ha ha ha Please Input Seat Number",Toast.LENGTH_LONG);
            tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
            tstMsg.show();
        }
    }


    public void sem_4(View v) {
        if(etText.getText().toString().length()>0) {
        lstExam = "94";
        txtSeatNo = etText.getText().toString().replaceFirst("^0+(?!$)", "");
        url = "http://www.gujaratuniversity.org.in/result_e/result/ex94/exam_chk.asp";
        new PostJSONTask().execute();
        }else {
            Toast tstMsg = Toast.makeText(getApplicationContext(),"Ha ha ha Please Input Seat Number",Toast.LENGTH_LONG);
            tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
            tstMsg.show();
        }
    }

    public void sem_5(View v) {
        if(etText.getText().toString().length()>0) {
        lstExam = "95";
        txtSeatNo = etText.getText().toString().replaceFirst("^0+(?!$)", "");
        url = "http://www.gujaratuniversity.org.in/result_e/result/ex95/exam_chk.asp";
        new PostJSONTask().execute();
        }else {
            Toast tstMsg = Toast.makeText(getApplicationContext(),"Ha ha ha Please Input Seat Number",Toast.LENGTH_LONG);
            tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
            tstMsg.show();
        }
    }

    public void sem_6(View v) {
        if(etText.getText().toString().length()>0) {
        lstExam = "96";
        txtSeatNo = etText.getText().toString().replaceFirst("^0+(?!$)", "");
        url = "http://www.gujaratuniversity.org.in/result_e/result/ex96/exam_chk.asp";
        Log.i(TAG, "Instance created : " + url);
        new PostJSONTask().execute();
        }else {
            Toast tstMsg = Toast.makeText(getApplicationContext(),"Ha ha ha Please Input Seat Number",Toast.LENGTH_LONG);
            tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
            tstMsg.show();
        }

    }



    private class PostJSONTask extends AsyncTask<String, String, String> {
        final String TAG = PostJSONTask.class.getSimpleName();

        ProgressBar spinner = findViewById(R.id.spinner);
        int responseCode;
        URL obj;
        StringBuilder tokenUri;
        HttpURLConnection con = null;
        private final String USER_AGENT = "Mozilla/5.0";

        @Override
        protected String doInBackground(String... params) {

            try {
                tokenUri = new StringBuilder("cmdSearch=");
                tokenUri.append(URLEncoder.encode("Search", "UTF-8"));
                tokenUri.append("&lstExam=");
                tokenUri.append(URLEncoder.encode(lstExam, "UTF-8"));
                tokenUri.append("&txtSeatNo=");
                tokenUri.append(URLEncoder.encode(txtSeatNo, "UTF-8"));

                Log.i(TAG ,"Parameter converted");
                obj = new URL(url);

                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("HEAD");

                    Log.i(TAG ,"URL active");
                    con.setRequestMethod("POST");
                    con.setRequestProperty("User-Agent", USER_AGENT);
                    con.setRequestProperty("Accept-Language", "UTF-8");

                    con.setDoOutput(true);

                    Log.i(TAG ,"Ready to take responce");
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(con.getOutputStream());
                    outputStreamWriter.write(tokenUri.toString());
                    outputStreamWriter.flush();

                    responseCode = con.getResponseCode();
                    Log.i(TAG ,"Responce taken");
                    System.out.println("\nSending 'POST' request to URL : " + url);
                    System.out.println("Post parameters : " + tokenUri);
                    System.out.println("Response Code : " + responseCode);
                    Log.i(TAG ,"Writing string");
                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    response.setLength(0);
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine + "\n");
                    }
                    in.close();
                    System.out.println((response));

                    Log.i(TAG ,"end of String");

            }catch (Exception e) {
                Log.e(TAG, "exception : Error at the connection");
            }
            return response.toString();

        }


        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            dialog = ProgressDialog.show(Results.this, "", "Loading...", true);


//            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.print(TAG + s);
            dialog.dismiss();
            student = new StringBuffer(response.toString());
            startActivity(new Intent(Results.this, ShowResult.class));
        }
    }
}



