package com.GUArena.BCA;


import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;

import android.view.View;
import android.widget.GridLayout;
import android.widget.Toast;

import static com.GUArena.BCA.BCA_FrontPage.isConnected;

public class ResultAll extends AppCompatActivity {

    private static final String TAG = ResultAll.class.getSimpleName();
    GridLayout   resultCard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isConnected(this)) {
            AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                    this);
            alertDialog2.setTitle(R.string.warning_alert3);


            alertDialog2.setPositiveButton(R.string.warning_alert4,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            finish();
                            startActivity(new Intent(ResultAll.this,BCA_FrontPage.class));
                        }
                    });
            alertDialog2.show();
        }
        setContentView(R.layout.results_all);
        Log.i(TAG , "Instance created");
        resultCard = (GridLayout) findViewById(R.id.result_grid);
        buttonInAction(resultCard);
    }

    @Override
    protected void  onRestart() {
        super.onRestart();
        defaultBG(resultCard);
    }


    private void defaultBG(final  GridLayout mainGrid){
        for(int i=0; i < mainGrid.getChildCount(); i++){
            final CardView btn = (CardView) mainGrid.getChildAt(i);
            btn.setCardBackgroundColor(Color.parseColor(String.valueOf("white")));
        }
    }
    @SuppressLint("ResourceAsColor")
    private void buttonInAction (GridLayout mainGrid){
        for(int i=0; i < mainGrid.getChildCount(); i++){
            final CardView btn = (CardView) mainGrid.getChildAt(i);
            final int FI = i+1;
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btn.setCardBackgroundColor(R.color.btnBackground);
                    switch (FI){
                        case 1:
                            Log.i(TAG,"Result 1 Clicked4");
                            Results.lay = 1;
                            startActivity(new Intent(ResultAll.this , Result1_New.class));
                            break;
                        case 2:
                            Log.i(TAG,"Result 2 Clicked4");
                            Results.lay = 2;
                            startActivity(new Intent(ResultAll.this , Result2_New.class));

                            break;
                        case 3:
                            Log.i(TAG,"other  result Clicked4");
                            Results.lay = 3;
                            startActivity(new Intent(ResultAll.this , OtherResult.class));
                            break;
                        case 4:
                            Log.i(TAG,"Semester 4 result");
                            Results.lay = 4;
                            startActivity(new Intent(ResultAll.this , Results.class));
                            break;
                        case 5:
                            Log.i(TAG,"Semester 5 result");
                            Results.lay = 5;
                            startActivity(new Intent(ResultAll.this , Results.class));
                            break;
                        case 6:
                            Log.i(TAG,"Semester 6 result");
                            Results.lay = 6;
                            startActivity(new Intent(ResultAll.this , Results.class));
                            break;

                        default:
                            Log.e(TAG , "Default miracle");

                    }

                }
            });

        }
    }
}



