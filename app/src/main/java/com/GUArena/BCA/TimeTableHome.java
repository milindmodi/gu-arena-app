package com.GUArena.BCA;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import static com.GUArena.BCA.BCA_FrontPage.isConnected;


public class TimeTableHome extends AppCompatActivity {
    private static final String TAG = SyllabusHome.class.getSimpleName();
    private static final String SYS = "SYS";
    private ListView syllabus;
    int totalSchedules = 0;
    ArrayList <String> title= new ArrayList<>();
    ArrayList <String> urls = new ArrayList<>();
    ArrayList <String> label = new ArrayList<>();
    ArrayList <String> allInOne = new ArrayList<String>();
    Document doc=null;
    Connection conn=null;
    TextView homepage;
    String selectedCourse;
    //new vars
    ArrayList <String> dropdown = new ArrayList<String>();
    Spinner spinner1;
    HashMap<String,String> listMap = new HashMap<String, String>();
    HashMap<String,String> clickListMap = new HashMap<String, String>();
    private ArrayAdapter<String> adapter;
    int pdfNum;
    String fileName;
    @Override
    protected void onDestroy() {
        super.onDestroy();
         finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title= new ArrayList<>();
        urls = new ArrayList<>();
        label = new ArrayList<>();
        allInOne = new ArrayList<>();
        setContentView(R.layout.syllabus_home);
        syllabus = findViewById(R.id.syllabus_list);
        homepage = findViewById(R.id.home_title);
        homepage.setText(R.string.timeTableButton);
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                System.out.print(" Seletced "+ id);
                // your code here
                int tmp = (int)(id);
                Log.i(TAG , "Selection change "+ id );
                Log.i(TAG , "Selection Text "+ dropdown.get(tmp) );
                Log.i(TAG,"Subject Code :" + listMap.get(dropdown.get(tmp)));
                selectedCourse = listMap.get(dropdown.get(tmp));
                title.clear();
                urls.clear();
                new timeTableList().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
                System.out.print("Nothing Selected");
            }

        });
        syllabus.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pdfNum=position;
//                Intent pdf = new Intent(Intent.ACTION_VIEW,Uri.parse(urls.get(pdfNum).toString()));
//                pdf.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                pdf.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                try{
//                    Toast.makeText(TimeTableHome.this,"Opening pdf ",Toast.LENGTH_LONG).show();
//                    startActivity(pdf);
//                }catch (Exception e){
//                    Log.e(TAG , "Error : in opening PDF" );
//                    Log.e(TAG , " "+urls.get(pdfNum));
//                }
//                //  Toast.makeText(SyllabusHome.this," "+position +" "+" "+strArr2.get(pdfNum) , Toast.LENGTH_LONG).show();
//                Log.e(TAG , "Error : Clicked PDF" + " "+position +" "+" "+urls.get(pdfNum));
                fileName = urls.get(pdfNum).toString();
                new DownloadPdf().execute();
            }
        });
        if (!isConnected(this)) {
            AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                    this);
            alertDialog2.setTitle(R.string.warning_alert3);


            alertDialog2.setPositiveButton(R.string.warning_alert4,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            finish();
                            startActivity(new Intent(TimeTableHome.this,BCA_FrontPage.class));
                        }
                    });
            alertDialog2.show();
        }
        //allInOne = savedInstanceState.getStringArrayList("allInOne");
        title.clone();
        urls.clear();
        new timeTableUpdate().execute();

//        syllabus.setOnItemClickListener(new OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                pdfNum=position;
//                Intent pdf = new Intent(Intent.ACTION_VIEW,Uri.parse(urls.get(pdfNum).toString()));
//                pdf.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                pdf.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                try{
//                    Toast.makeText(TimeTableHome.this,"Opening pdf ",Toast.LENGTH_LONG).show();
//                    startActivity(pdf);
//                }catch (Exception e){
//                    Log.e(TAG , "Error : in opening PDF" );
//                    Log.e(TAG , " "+urls.get(pdfNum));
//                }
//                //  Toast.makeText(SyllabusHome.this," "+position +" "+" "+strArr2.get(pdfNum) , Toast.LENGTH_LONG).show();
//                Log.e(TAG , "Error : Clicked PDF" + " "+position +" "+" "+urls.get(pdfNum));
//            }
//        });
    }

    public void addItemsOnSpinner1() {





        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, dropdown);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter);
    }
    Connection.Response rs;

    private class timeTableUpdate extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... param) {

            try{
                rs =  Jsoup
                        .connect(BCA_FrontPage.GUJ_URL_TIMETABLE)
                        .method(Connection.Method.GET)
                        .userAgent("Mozilla/5.0")
                        .timeout(60000)
                        .validateTLSCertificates(false)
                        .execute();


                doc = rs.parse();

                System.out.print("Code  "+rs.cookie("ci_session"));
                System.out.print("Code  "+doc);
                Elements links = doc.select("option");
                System.out.print("List "+links);
                int i =0;
                for (Element link : links) {
                    listMap.put(link.text(),link.attr("value"));
                    System.out.print("List "+link);
                    dropdown.add(link.text());
                }

            }catch (Exception e){
                System.out.print("Error " + e);
            }
            System.out.println("\nList complete" + "  "+totalSchedules);
            return  null;
        }
        Dialog dialog ;

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
           dialog.dismiss();
           addItemsOnSpinner1();

        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            dialog = ProgressDialog.show(TimeTableHome.this, "", "Loading");
        }
    }
    private class timeTableList extends AsyncTask<String, String, String> {
        Connection.Response rs;
        @Override
        protected String doInBackground(String... param) {

            try{

                Elements links = doc.select("li[exam-id="+selectedCourse+"] > div > div > div > div > a");
//                System.out.print("List "+links);
//                int i =0;
                for (Element link : links) {
//                    listMap.put(link.text(),link.attr("value"));
                    System.out.println("List "+link.attr("href").toString());
                    System.out.println("List "+link.text());
//                    Element tmp = link.select("a[href]").first();
                    title.add(link.text());
                    urls.add(link.attr("href").toString());

//                    System.out.print(" List "+link.childNode(1).childNode(1).childNode(1).childNode(1).childNode(2).toString());
//                    System.out.print(" Link "+link.childNode(1).childNode(1).childNode(1).childNode(3).childNode(1).attr("href").toString());
//                    dropdown.add(link.text());
                }

            }catch (Exception e){
                System.out.print("Error " + e);
            }
            System.out.println("\nList complete" + "  "+totalSchedules);
            return  null;
        }
        Dialog dialog ;

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1,title){
                @Override
                public View getView(int position, View convertView, ViewGroup parent){
                    // Get the Item from ListView
                    View view = super.getView(position, convertView, parent);

                    // Initialize a TextView for ListView each Item
                    TextView tv = (TextView) view.findViewById(android.R.id.text1);

                    // Set the text color of TextView (ListView Item)
                    tv.setTextColor(Color.BLUE);

                    // Generate ListView Item using TextView
                    return view;
                }
            };
            syllabus.setAdapter(adapter);
            dialog.dismiss();

        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();

            dialog = ProgressDialog.show(TimeTableHome.this, "", "Loading");
        }
    }
    private class DownloadPdf extends AsyncTask<String, String, String> {

        private static final int  MEGABYTE = 1024 * 1024;
        @Override
        protected String doInBackground(String... param) {
            fileName = urls.get(pdfNum).toString();
            File outputFile = null;
            InputStream input =null;
            try {
                Log.i("Got here" ,"dfzdc");
                //USing Jsoup
                Connection.Response rs2 =   Jsoup
                        .connect(fileName)
                        .ignoreContentType(true)
                        .maxBodySize(1024*1024*10*2)
                        .userAgent("Mozilla/5.0")
                        .timeout(60000)
                        .validateTLSCertificates(false)
                        .execute();
                String myDownloadPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+File.separator + "GU_Arena";
                File downloadPath = new File(myDownloadPath);
                if (!downloadPath.exists()) {
                    downloadPath.mkdirs();
                    Log.i(TAG,"Path creted");
                }
                String fileName1 =title.get(pdfNum)  +".pdf";

                int len = rs2.bodyAsBytes().length;
                FileOutputStream out =  new FileOutputStream(new File(myDownloadPath,fileName1));
                out.write(rs2.bodyAsBytes(),0,len);
                out.close();


            }

            catch (Exception e) {
                Log.i(TAG,"Error " + e + fileName);
            }
            return null;
        }

        Dialog dialog;

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            dialog.dismiss();
            String myDownloadPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+File.separator + "GU_Arena";

            String fileName1 =title.get(pdfNum) +".pdf";
            File file = new File(myDownloadPath,fileName1);
//            Log.i(TAG,"PAth :" + Uri.fromFile(file));
//            Intent viewFile = new Intent(Intent.ACTION_VIEW);
//            viewFile.setDataAndType(Uri.fromFile(file), "application/pdf");
//            viewFile.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            viewFile.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//
            Intent intent = null;
//            startActivity(intent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Log.i(TAG,"In if");
                Uri uri = FileProvider.getUriForFile(getApplicationContext(), "com.mydomain.fileprovider" , file);
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(uri);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            } else {
                Log.i(TAG,"In else");
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                intent = Intent.createChooser(intent, "Open File");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = ProgressDialog.show(TimeTableHome.this, "", "Loading");
        }
    }
}
