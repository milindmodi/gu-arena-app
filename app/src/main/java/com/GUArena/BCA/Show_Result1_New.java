package com.GUArena.BCA;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Show_Result1_New extends AppCompatActivity {
    TextView showResult ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show__result1__new);
        showResult  = findViewById(R.id.viewResult);
        showResult();
    }
    private void showResult() {

        String rslt = Result1_New.student.toString();

        if (rslt.isEmpty()) {

            showResult.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            showResult.setText(R.string.no_result);
            Toast tstMsg = Toast.makeText(getApplicationContext(),"Result is not uploaded yet",Toast.LENGTH_LONG);
            tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
            tstMsg.show();

        }else if(rslt.indexOf("Invalid Seat Number.") != -1) {
            showResult.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            showResult.setText(R.string.valid_seatNo);
            Toast tstMsg = Toast.makeText(getApplicationContext(),"Enter Valid Seat Number",Toast.LENGTH_LONG);
            tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
            tstMsg.show();
        } else if(rslt.indexOf("Incorrect Captcha code.") != -1) {
            showResult.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            showResult.setText(R.string.valid_seatNo);
            Toast tstMsg = Toast.makeText(getApplicationContext(),"Incorrect Captcha code.",Toast.LENGTH_LONG);
            tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
            tstMsg.show();
        }else {
            showResult.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            showResult.setText(rslt);
            Toast tstMsg = Toast.makeText(getApplicationContext(),"Rotate phone",Toast.LENGTH_LONG);
            tstMsg.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0 ,0 );
            tstMsg.show();
        }
    }
    private boolean isPermissionGranted() {
        int writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }
        if (writepermission == PackageManager.PERMISSION_GRANTED){
            return true;
        }else {
            return false;
        }
    }
    public void share(View v){
        if(isPermissionGranted()) {
            HorizontalScrollView sc = findViewById(R.id.scroll);
            sc.setDrawingCacheEnabled(false);
            Bitmap image = Bitmap.createBitmap(sc.getChildAt(0).getWidth(), sc.getChildAt(0).getHeight(), Bitmap.Config.ARGB_8888);
            Canvas b = new Canvas(image);
            Drawable d = sc.getBackground();
            b.drawColor(Color.WHITE);
            sc.draw(b);
            String extr = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+File.separator + "GU_Arena";
            String fileName ="Result_"+new SimpleDateFormat("yyyy'_report.jpg'").format(new Date());
            File myPath = new File(extr, fileName);
            OutputStream fos = null;
            try {
                fos = openFileOutput(extr+fileName, MODE_WORLD_READABLE);
                image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String uri = MediaStore.Images.Media.insertImage(getContentResolver(), image, fileName, fileName);
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            share.putExtra(Intent.EXTRA_STREAM, Uri.parse(uri));
            share.putExtra(Intent.EXTRA_TEXT, "Best application for Search & Share Results of Gujarat University Via https://play.google.com/store/apps/details?id=com.GUArena.BCA");
            startActivity(Intent.createChooser(share, "Share Result via"));
        }else   {
//            Toast.makeText(this,"Ask",Toast.LENGTH_SHORT).show();

        }
    }
}
