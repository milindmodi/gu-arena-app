package com.GUArena.BCA;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

public class ShowResult extends AppCompatActivity {


    private static final String TAG = ShowResult.class.getSimpleName();
    TextView showResult ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_result);
        showResult  = findViewById(R.id.viewResult);
        showResult();
    }


    public void share(View v){

    }

    private void showResult() {
        String rslt = Result1_New.student.toString();
            try {
                showResult.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                showResult.setText(rslt);

            }catch (Exception e){
                Log.i(TAG,"Something went wrong");
            }
    }
}

