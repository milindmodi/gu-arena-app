package com.GUArena.BCA;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import  android.support.v4.app.NotificationCompat;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static android.support.v4.app.NotificationCompat.DEFAULT_ALL;

public class AlertService extends Service {
    private static final String TAG = AlertService.class.getSimpleName();

    Switch mySwitch1;
    Switch mySwitch2;
    Switch mySwitch3;
    Switch mySwitch4;
    Switch mySwitch5;
    Switch mySwitch6;
    boolean NOTIFIED1=true;
    boolean NOTIFIED2=true;
    boolean NOTIFIED3=true;
    boolean NOTIFIED4=true;
    boolean NOTIFIED5=true;
    boolean NOTIFIED6=true;
    boolean silent1 ;
    boolean silent2 ;
    boolean silent3 ;
    boolean silent4 ;
    boolean silent5 ;
    boolean silent6 ;


public  void showNotification(String title){
      /*  NotificationHelper helper = new NotificationHelper(AlertService.this);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        Notification.Builder b = helper.getNotificationO(title,"click to see your result");
        helper.getManagerO().notify(new Random().nextInt(), b.build());
    }else{
        NotificationCompat.Builder b = helper.getNotification(title,"click to see your result");
        helper.getManager().notify(new Random().nextInt(), b.build());
    }*/

    NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(getApplicationContext(), "notify_001");
    Intent ii = new Intent(getApplicationContext(), ResultAll.class);
    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, ii, 0);

    NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
    bigText.bigText("Notification for the results");
    bigText.setBigContentTitle(title);
    bigText.setSummaryText("");

    mBuilder.setContentIntent(pendingIntent);
    mBuilder.setSmallIcon(R.mipmap.ic_launcher_round);
    mBuilder.setContentTitle(title);
    mBuilder.setPriority(Notification.PRIORITY_MAX);
    mBuilder.setStyle(bigText);

    NotificationManager mNotificationManager =
            (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);


    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        NotificationChannel channel = new NotificationChannel("notify_001",
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT);
        mNotificationManager.createNotificationChannel(channel);
    }
    mNotificationManager.notify(new Random().nextInt(), mBuilder.build());
}

        Timer time=new Timer();
        @Override
        public void onCreate() {
            super.onCreate();
           /* try {
                time.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {


                        int totalSchedules = 0;
                        Document doc=null;
                        try {
                            doc =  Jsoup.connect("http://result.gujaratuniversity.ac.in/").get();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        try {
                            Elements links = doc.select("select").select("option");

                            for(Element link : links) {

                                if(link.attr("value").toString().startsWith("9")) {
                                    if(silent1  && NOTIFIED1 &&  link.attr("value").toString().equals("91")){

                                        try{
                                              showNotification("Semester 1 Results are out");

                                        }catch (Exception e){
                                            System.out.println("Sem 1 calling error");
                                            e.printStackTrace();
                                        }finally {
                                            Log.i(TAG , "SEM 1 time");
                                            NOTIFIED1=false;
                                        }

                                    }else {
                                        Log.e(TAG , "SEM 1 not found time");

                                    }
                                    if(silent2 && NOTIFIED2 && link.attr("value").toString().equals("92")){

                                        try{
                                            showNotification("Semester 2 Results are out");

                                        }catch (Exception e){
                                            System.out.println("Sem 2 calling error");
                                            e.printStackTrace();
                                        }finally {
                                            Log.i(TAG , "SEM 2 time");
                                            NOTIFIED2=false;
                                        }
                                    }else {
                                        Log.e(TAG , "SEM 2 not found time");
                                    }
                                    if(silent3 && NOTIFIED3 && link.attr("value").toString().equals("93")) {
                                        try {
                                            showNotification("Semester 3 Results are out");

                                        } catch (Exception e) {
                                            System.out.println("Sem 3 calling error");
                                            e.printStackTrace();
                                        } finally {
                                            Log.i(TAG, "SEM 3 time");
                                            NOTIFIED3 = false;
                                        }
                                    }else {
                                        Log.e(TAG , "SEM 3 not found time");
                                    }
                                    if(silent4 && NOTIFIED4 && link.attr("value").toString().equals("94")){
                                        try {
                                            showNotification("Semester 4 Results are out");

                                        } catch (Exception e) {
                                            System.out.println("Sem 4 calling error");
                                            e.printStackTrace();
                                        } finally {
                                            Log.i(TAG, "SEM 4 time");
                                            NOTIFIED4 = false;
                                        }
                                    }
                                    else {
                                        Log.e(TAG , "SEM 4 not found time");
                                    }
                                    if(silent5 && NOTIFIED5 && link.attr("value").toString().equals("95")) {
                                        try {
                                            showNotification("Semester 5 Results are out");

                                        } catch (Exception e) {
                                            System.out.println("Sem 5 calling error");
                                            e.printStackTrace();
                                        } finally {
                                            Log.i(TAG, "SEM 5 time");
                                            NOTIFIED5 = false;
                                        }
                                    }else {
                                        Log.e(TAG , "SEM 5 not found time");
                                    }
                                    if(silent6 && NOTIFIED6 && link.attr("value").toString().equals("96")) {
                                        try {
                                            showNotification("Semester 6 Results are out");

                                        } catch (Exception e) {
                                            System.out.println("Sem 6 calling error");
                                            e.printStackTrace();
                                        } finally {
                                            Log.i(TAG, "SEM 6 time");
                                            NOTIFIED6 = false;
                                        }
                                    }else {
                                        Log.e(TAG , "SEM 6 not found time");
                                    }
                                    System.out.println(link.text());
                                }
                                totalSchedules++;
                            }
                            System.out.println("\nList complete" + "  "+totalSchedules);
                            Log.i(TAG, "in run method 1");
                        }catch(Exception e) {
                            System.out.println("Some thing happedned in syllabus");
                        }
                     //   showNotification("Juut chilling");
                    }
                }, 500, 10000);
            } catch (Exception e) {
                e.printStackTrace();
            }*/

        }



    private static final String PREFS_NAME = "PREFS_NAME";
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        silent1 = settings.getBoolean("switchkey1", false);
        silent2 = settings.getBoolean("switchkey2", false);
        silent3 = settings.getBoolean("switchkey3", false);
        silent4 = settings.getBoolean("switchkey4", false);
        silent5 = settings.getBoolean("switchkey5", false);
        silent6 = settings.getBoolean("switchkey6", false);
        Log.i( TAG,"semester switch Values are fetched");

        Toast.makeText(AlertService.this, "CREATED", Toast.LENGTH_LONG).show();
        return  START_STICKY;
    }

    @Override
    public void onDestroy() {
        time.cancel();
        Log.e( TAG,"Destroyed");
     //   Toast.makeText(AlertService.this,"Canceled",Toast.LENGTH_LONG).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
