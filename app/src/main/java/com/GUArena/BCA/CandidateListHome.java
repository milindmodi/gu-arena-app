package com.GUArena.BCA;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import static com.GUArena.BCA.BCA_FrontPage.isConnected;


public class CandidateListHome extends AppCompatActivity {
    private static final String TAG = SyllabusHome.class.getSimpleName();
    private static final String SYS = "SYS";
    private ListView syllabus;
    int totalSchedules = 0;
    Dialog dialog ;
    ArrayList <String> title= new ArrayList<>();
    ArrayList <String> urls = new ArrayList<>();
    ArrayList <String> label = new ArrayList<>();
    ArrayList <String> allInOne = new ArrayList<>();
    Document doc=null;
    Connection conn=null;
    TextView homepage;
    String selectedCourse;
    ArrayList <String> dropdown = new ArrayList<String>();
    Spinner spinner1;
    HashMap<String,String> listMap = new HashMap<String, String>();
    HashMap<String,String> clickListMap = new HashMap<String, String>();
    private ArrayAdapter<String> adapter;
    int pdfNum;
    String fileName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.syllabus_home);
        title= new ArrayList<>();
        urls = new ArrayList<>();
        label = new ArrayList<>();
        allInOne = new ArrayList<>();
        setContentView(R.layout.syllabus_home);
        syllabus = findViewById(R.id.syllabus_list);
        homepage = findViewById(R.id.home_title);
        homepage.setText(R.string.candidateListButton);
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        new timeTableUpdate().execute();
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                System.out.print(" Seletced "+ id);
                // your code here
                int tmp = (int)(id);
                Log.i(TAG , "Selection change "+ id );
                Log.i(TAG , "Selection Text "+ dropdown.get(tmp) );
                Log.i(TAG,"Subject Code :" + listMap.get(dropdown.get(tmp)));
                selectedCourse = listMap.get(dropdown.get(tmp));
                title.clear();
                urls.clear();
                new timeTableList().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
                System.out.print("Nothing Selected");
            }

        });
        if (!isConnected(this)) {
            AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                    this);
            alertDialog2.setTitle(R.string.warning_alert3);


            alertDialog2.setPositiveButton(R.string.warning_alert4,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            finish();
                            startActivity(new Intent(CandidateListHome.this,BCA_FrontPage.class));
                        }
                    });
            alertDialog2.show();
        }


        //allInOne = savedInstanceState.getStringArrayList("allInOne");


        syllabus.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pdfNum=position;
                fileName = urls.get(pdfNum).toString();
                new DownloadPdf().execute();
//                Intent pdf = new Intent(Intent.ACTION_VIEW,Uri.parse(urls.get(pdfNum).toString()));
//                pdf.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                pdf.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                try{
//                    Toast.makeText(CandidateListHome.this,"Opening pdf ",Toast.LENGTH_LONG).show();
//                    startActivity(pdf);
//                }catch (Exception e){
//                    Log.e(TAG , "Error : in opening PDF" );
//                    Log.e(TAG , " "+urls.get(pdfNum));
//                }
//                //  Toast.makeText(SyllabusHome.this," "+position +" "+" "+strArr2.get(pdfNum) , Toast.LENGTH_LONG).show();
//                Log.e(TAG , "Error : Clicked PDF" + " "+position +" "+" "+urls.get(pdfNum));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    public void addItemsOnSpinner1() {





        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, dropdown);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(dataAdapter);
    }
    Connection.Response rs;
    private class timeTableUpdate extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... param) {

            try {
                rs = Jsoup
                        .connect(BCA_FrontPage.GUJ_URL_CANDIDATE)
                        .method(Connection.Method.GET)
                        .userAgent("Mozilla/5.0")
                        .timeout(60000)
                        .validateTLSCertificates(false)
                        .execute();


                doc = rs.parse();

                System.out.print("Code  " + rs.cookie("ci_session"));
                System.out.print("Code  " + doc);
                Elements links = doc.select("option");
                System.out.print("List " + links);
                int i = 0;
                for (Element link : links) {
                    listMap.put(link.text(), link.attr("value"));
                    System.out.print("List " + link);
                    dropdown.add(link.text());
                }

            } catch (Exception e) {
                System.out.print("Error " + e);
            }
            System.out.println("\nList complete" + "  " + totalSchedules);
            return null;
        }

        Dialog dialog;

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            addItemsOnSpinner1();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(CandidateListHome.this, "", "Loading");
        }
    }

    private class timeTableList extends AsyncTask<String, String, String> {
        Connection.Response rs;

        @Override
        protected String doInBackground(String... param) {

            try {

                Elements links = doc.select("li[exam-id=" + selectedCourse + "] > div > div > div > div > a");
                for (Element link : links) {
                    System.out.println("List " + link.attr("href").toString());
                    System.out.println("List " + link.text());
                    title.add(link.text());
                    urls.add(link.attr("href").toString());

                }

            } catch (Exception e) {
                System.out.print("Error " + e);
            }
            System.out.println("\nList complete" + "  " + totalSchedules);
            return null;
        }

        Dialog dialog;

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1,title){
                @Override
                public View getView(int position, View convertView, ViewGroup parent){
                    // Get the Item from ListView
                    View view = super.getView(position, convertView, parent);

                    // Initialize a TextView for ListView each Item
                    TextView tv = (TextView) view.findViewById(android.R.id.text1);

                    // Set the text color of TextView (ListView Item)
                    tv.setTextColor(Color.BLUE);

                    // Generate ListView Item using TextView
                    return view;
                }
            };
            syllabus.setAdapter(adapter);
            dialog.dismiss();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = ProgressDialog.show(CandidateListHome.this, "", "Loading");
        }
    }
    private class DownloadPdf extends AsyncTask<String, String, String> {

        private static final int  MEGABYTE = 1024 * 1024;
        @Override
        protected String doInBackground(String... param) {
            fileName = urls.get(pdfNum).toString();
            File outputFile = null;
            InputStream input =null;
            try {
                Log.i("Got here" ,"dfzdc");
                //USing Jsoup
                Connection.Response rs2 =   Jsoup
                        .connect(fileName)
                        .ignoreContentType(true)
                        .maxBodySize(1024*1024*10*2)
                        .userAgent("Mozilla/5.0")
                        .timeout(60000)
                        .validateTLSCertificates(false)
                        .execute();
                String myDownloadPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+File.separator + "GU_Arena";
                File downloadPath = new File(myDownloadPath);
                if (!downloadPath.exists()) {
                    downloadPath.mkdirs();
                    Log.i(TAG,"Path creted");
                }
                String fileName1 =title.get(pdfNum)  +".txt";

                int len = rs2.bodyAsBytes().length;
                FileOutputStream out =  new FileOutputStream(new File(myDownloadPath,fileName1));
                out.write(rs2.bodyAsBytes(),0,len);
                out.close();


            }

            catch (Exception e) {
                Log.i(TAG,"Error " + e + fileName);
            }
            return null;
        }

        Dialog dialog;

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            dialog.dismiss();
            String myDownloadPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+File.separator + "GU_Arena";

            String fileName1 =title.get(pdfNum) +".txt";
            File file = new File(myDownloadPath,fileName1);
//            Log.i(TAG,"PAth :" + Uri.fromFile(file));
//            Intent viewFile = new Intent(Intent.ACTION_VIEW);
//            viewFile.setDataAndType(Uri.fromFile(file), "application/pdf");
//            viewFile.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            viewFile.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//
            Intent intent = null;
//            startActivity(intent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Log.i(TAG,"In if");
                Uri uri = FileProvider.getUriForFile(getApplicationContext(), "com.mydomain.fileprovider" , file);
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(uri);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            } else {
                Log.i(TAG,"In else");
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "text/plain");
                intent = Intent.createChooser(intent, "Open File");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = ProgressDialog.show(CandidateListHome.this, "", "Loading");
        }
    }
}


//    private class CandidateUpdate extends AsyncTask<String, String, String> {
//
//        public Document content(){
//            try {
//
//                doc =  Jsoup.connect("http://www.gujaratuniversity.ac.in/web/custom/student/candidate-list").get();
//                return doc;
//            } catch (Exception e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//                return  content();
//            }
//        }
//        @Override
//        protected String doInBackground(String... param) {
//
//           doc = content();
//
//            Elements links = doc.select("li");
//            try {
//
//                for (Element link : links) {
//
//                    //	urls.add(link.childNode(1).childNode(1).childNode(1).childNode(3).childNode(1).attr("href").toString());
//
//                    //System.out.println(link.attr("exam-id"));
//                    if (link.attr("exam-id").toString().equals("9")) {
//
//                        int firstSpace = link.text().toString().indexOf(" ");
//                        if (firstSpace != -1)
//                            label.add("[ " + (link.text().toString().substring(0, firstSpace) + " ]\n"));
//                        System.out.println(label.get(totalSchedules));
//
//                        title.add(link.text().toString().substring(firstSpace, link.text().length()));
//                        System.out.println(title.get(totalSchedules));
//
//
//                        urls.add(link.childNode(1).childNode(1).childNode(1).childNode(3).childNode(1).attr("href").toString());
//                        System.out.println(urls.get(totalSchedules));
//                        //.add(link.childNode(1).childNode(1).childNode(1).childNode(3).childNode(1).);
//
//                        allInOne.add(label.get(totalSchedules) + title.get(totalSchedules));
//                        totalSchedules++;
//                    }
//
//                }
//
//
//            }catch(Exception e) {
//                System.out.println("Some thing happedned in SyllabusHomme");
//            }
//            System.out.println("\nList complete" + "  "+totalSchedules);
//            return  null;
//        }
//
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1,allInOne);
//            syllabus.setAdapter(adapter);
//           dialog.dismiss();
//
//        }
//
//        @Override
//        protected void onPreExecute(){
//            super.onPreExecute();
//          dialog = ProgressDialog.show(CandidateListHome.this, "", getResources().getString(R.string.loading));
//          dialog.show();
//        }
//    }
//}
