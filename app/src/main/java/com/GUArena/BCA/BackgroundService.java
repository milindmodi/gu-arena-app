package com.GUArena.BCA;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.widget.Switch;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class BackgroundService extends Service {


    boolean NOTIFIED1 = true;
    boolean NOTIFIED2 = true;
    boolean NOTIFIED3 = true;
    boolean NOTIFIED4 = true;
    boolean NOTIFIED5 = true;
    boolean NOTIFIED6 = true;
    boolean silent1;
    boolean silent2;
    boolean silent3;
    boolean silent4;
    boolean silent5;
    boolean silent6;


    Timer time = new Timer();

    private static final String PREFS_NAME = "PREFS_NAME";
    private static final String TAG = BackgroundService.class.getSimpleName();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        BCA_FrontPage.isbgServiceOn = true;
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor ed = settings.edit();
        ed.putBoolean("isbgServiceOn", BCA_FrontPage.isbgServiceOn);
        ed.commit();
        silent1 =  (settings.getBoolean("switchkey1",false));
        silent2 = (settings.getBoolean("switchkey2",false));
        silent3 = (settings.getBoolean("switchkey3",false));
        silent4 =  (settings.getBoolean("switchkey4",false));
        silent5 =  (settings.getBoolean("switchkey5",false));
        silent6 =  (settings.getBoolean("switchkey6",false));

        NOTIFIED1 =  (settings.getBoolean("NOTIFIED1",true));
        NOTIFIED2 =  (settings.getBoolean("NOTIFIED2",true));
        NOTIFIED3 =  (settings.getBoolean("NOTIFIED3",true));
        NOTIFIED4 =  (settings.getBoolean("NOTIFIED4",true));
        NOTIFIED5 =  (settings.getBoolean("NOTIFIED5",true));
        NOTIFIED6 =  (settings.getBoolean("NOTIFIED6",true));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BCA_FrontPage.isbgServiceOn = false;
        System.out.print("LEVAI GYO");
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor ed = settings.edit();
        ed.putBoolean("isbgServiceOn", BCA_FrontPage.isbgServiceOn);
        ed.commit();
        commitWork();
        Toast.makeText(this, "BG saved", Toast.LENGTH_SHORT).show();
    }
    private void commitWork() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("switchkey1", silent1);
        editor.putBoolean("switchkey2", silent2);
        editor.putBoolean("switchkey3", silent3);
        editor.putBoolean("switchkey4", silent4);
        editor.putBoolean("switchkey5", silent5);
        editor.putBoolean("switchkey6", silent6);
        editor.commit();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.print("CALLED");
        Log.i(TAG, "Service created");

        Intent i = new Intent(this, AlertHome.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(BackgroundService.this, 0, i, 0);
        Notification backgroundNotification = new NotificationCompat.Builder(BackgroundService.this, "0")
                .setContentTitle("Checking for result every Minute")
                .setContentText("if you want to Remove notification you have to disable alert feature")
                .setSmallIcon(R.drawable.ic_baseline_loop_24px)
                .setContentIntent(pendingIntent)
                .build();
        //notification then disable all alerts")
        Log.i(TAG, "NOTIFICATION created");

        System.out.print("NOTIFICATION CREATED");
        startForeground(1, backgroundNotification);
        try {
            time.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    System.out.print("EXECUTED");
                    fetching();
                    checkingResults();
                    commitWork();
                    //
//                     showNotification("Chilll");
                    Log.i(TAG, "EXECUTING BACKGROUND");
                }//60000
            }, 500, 60000);

        } catch (Exception e) {
            System.out.print("LEVAI GYO ERROR");
            e.printStackTrace();
        }
        return START_STICKY;


    }

    private void fetching() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        silent1 =  (settings.getBoolean("switchkey1",false));
        silent2 = (settings.getBoolean("switchkey2",false));
        silent3 = (settings.getBoolean("switchkey3",false));
        silent4 =  (settings.getBoolean("switchkey4",false));
        silent5 =  (settings.getBoolean("switchkey5",false));
        silent6 =  (settings.getBoolean("switchkey6",false));
    }

    private void checkingResults() {
        int totalSchedules = 0;
        Document doc = null;
        try {
            doc = Jsoup.connect("http://result.gujaratuniversity.ac.in/").get();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            Elements links = doc.select("select").select("option");

            for (Element link : links) {

                if (  link.attr("value").toString().startsWith("9")) {
                    if (NOTIFIED1 && silent1 && link.attr("value").toString().equals("91")) {
                        try {
                            showNotification("Semester 1 Results are out");
                            silent1 = false;
                        } catch (Exception e) {
                            System.out.println("Sem 1 calling error");
                            e.printStackTrace();
                        } finally {
                            Log.i(TAG, "FINAL SEM 1 time");
                        }
                    } else {
                        Log.e(TAG, "SEM 1 not found time");

                    }
                    if ( NOTIFIED2 && silent2 && link.attr("value").toString().equals("92")) {

                        try {
                            showNotification("Semester 2 Results are out");
                            silent2 = false;

                        } catch (Exception e) {
                            System.out.println("Sem 2 calling error");
                            e.printStackTrace();
                        } finally {
                            Log.i(TAG, "SEM 2 time");
                        }
                    } else {
                        Log.e(TAG, "SEM 2 not found time");
                    }
                    if (NOTIFIED3 && silent3 &&  link.attr("value").toString().equals("93")) {
                        try {
                            showNotification("Semester 3 Results are out");
                            silent3 = false;
                        } catch (Exception e) {
                            System.out.println("Sem 3 calling error");
                            e.printStackTrace();
                        } finally {
                            Log.i(TAG, "SEM 3 time");
                        }
                    } else {
                        Log.e(TAG, "SEM 3 not found time");
                    }
                    if (silent4 && NOTIFIED4 && link.attr("value").toString().equals("94")) {
                        try {
                            showNotification("Semester 4 Results are out");
                            silent4 =  false;
                        } catch (Exception e) {
                            System.out.println("Sem 4 calling error");
                            e.printStackTrace();
                        } finally {
                            Log.i(TAG, "SEM 4 time");
                        }
                    } else {
                        Log.e(TAG, "SEM 4 not found time");
                    }
                    if (silent5 && NOTIFIED5 && link.attr("value").toString().equals("95")) {
                        try {
                            showNotification("Semester 5 Results are out");
                            silent5 = false;
                        } catch (Exception e) {
                            System.out.println("Sem 5 calling error");
                            e.printStackTrace();
                        } finally {
                            Log.i(TAG, "SEM 5 time");
                        }
                    } else {
                        Log.e(TAG, "SEM 5 not found time");
                    }
                    if (NOTIFIED6 && silent6 && link.attr("value").toString().equals("96")) {
                        try {
                            showNotification("Semester 6 Results are out");
                            Log.i(TAG, "USER NOTITFIED");
                          silent6 = false;
                        } catch (Exception e) {
                            System.out.println("Sem 6 calling error");
                            e.printStackTrace();
                        } finally {
                            Log.i(TAG, "SEM 6 time");
                        }
                    } else {
                        Log.e(TAG, "SEM 6 not found time");
                    }
                    System.out.println(link.text());
                }
                totalSchedules++;
            }
            System.out.println("\nList complete" + "  " + totalSchedules);
            Log.i(TAG, "in run method 1");
        } catch (Exception e) {
            System.out.println("Some thing happedned in syllabus");
        }
        //   showNotification("Juut chilling");
    }




    public void showNotification(String title) {
        System.out.print("CALLED");
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "0");
        Intent ii = new Intent(getApplicationContext(), ResultAll.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, ii, 0);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText("Notification for the results");
        bigText.setBigContentTitle(title);
        bigText.setSummaryText("");

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.ic_baseline_notifications_active_24px);
        mBuilder.setContentTitle(title);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);

        NotificationManager mNotificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("notify",
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
        }
        mNotificationManager.notify(new Random().nextInt(), mBuilder.build());
    }

}

